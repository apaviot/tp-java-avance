package fr.umlv.rental;

import java.util.Calendar;

public class Camel extends AbstractVehicle {

	public Camel(int year) {
		super(year);
	}
	
	public int insuranceCost() {
		int age = Calendar.getInstance().get(Calendar.YEAR) - getYearOfCommissioning();
		return 100 * age;
	}
	
	@Override
	public String toString() {
		return "camel " + getYearOfCommissioning();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Camel))
			return false;
		return super.equals(obj);
	}
}
