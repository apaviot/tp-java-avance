package fr.umlv.rental;

abstract class AbstractVehicle implements Vehicle {
	private final int year;
	
	AbstractVehicle(int year) {
		this.year = year;
	}

	@Override
	public int getYearOfCommissioning() {
		return year;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (o == null)
			return false;
		
		AbstractVehicle other = (AbstractVehicle) o;
		return year == other.year;
	}
	
	@Override
	public int hashCode() {
		return year;
	}
}
