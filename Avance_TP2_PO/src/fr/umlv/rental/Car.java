package fr.umlv.rental;
import java.util.Calendar;
import java.util.Objects;

public class Car extends AbstractVehicle {

	private final String model;
	
	public Car(String model, int year) {
		super(year);
		this.model = Objects.requireNonNull(model);
	}

	public String getModel() {
		return model;
	}
	
	public int insuranceCost() {
		if (Calendar.getInstance().get(Calendar.YEAR) - getYearOfCommissioning() < 10)
			return 200;
		return 500;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + super.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Car))
			return false;
		Car other = (Car) obj;
		return super.equals(obj) && model.equals(other.model);
	}
	
	@Override
	public String toString() {
		return model + " " + getYearOfCommissioning();
	}
}
