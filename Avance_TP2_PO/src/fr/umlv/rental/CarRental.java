package fr.umlv.rental;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarRental {
	
	private final List<Vehicle> rentedVehicles;
	
	public CarRental() {
		rentedVehicles = new ArrayList<>();
	}
	
	public void add(Vehicle vehicle) {
		rentedVehicles.add(Objects.requireNonNull(vehicle));
	}
	
	public void remove(Vehicle vehicle) {
		if (!rentedVehicles.remove(Objects.requireNonNull(vehicle))) {
			throw new IllegalStateException();
		}
	}
	
	public Stream<Vehicle> stream() {
		return rentedVehicles.stream();
	}
	
	public List<Vehicle> findAllByYear(int year) {
		return rentedVehicles.stream().filter(c -> c.getYearOfCommissioning() == year).collect(Collectors.toList());
	}
	
	public int insuranceCost() {
		return rentedVehicles.stream().mapToInt(v -> v.insuranceCost()).sum();
	}
	
	public Optional<Car> findACarByModel(String model) {
		Objects.requireNonNull(model);
		return rentedVehicles.stream()
			.filter(c -> c instanceof Car)
			.map(c -> (Car) c)
			.filter(c -> c.getModel().equals(model))
			.findAny();
	}
	
	@Override
	public String toString() {
		return rentedVehicles.stream().map(c -> c.toString()).collect(Collectors.joining("\n"));
	}
}
