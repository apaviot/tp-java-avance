package fr.umlv.rental;

public interface Vehicle {
	int getYearOfCommissioning();
	
	int insuranceCost();
}
