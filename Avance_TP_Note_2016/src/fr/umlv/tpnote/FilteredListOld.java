package fr.umlv.tpnote;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilteredListOld<T> implements Iterable<T> {
	private Entry<T> head;
	private final Predicate<? super T> filter;
	
	public FilteredListOld() {
		this(e -> true);	
	}
	
	public FilteredList(Predicate<? super T> filter) {
		this.filter = Objects.requireNonNull(filter);
	}
	
	public void addFirst(T value) {
		head = new Entry<T>(value, head);
	}
	
	public void forEach(Consumer<? super T> consumer) {
		Objects.requireNonNull(consumer);
		Stream.iterate(head,  e -> e != null, e -> e.next).forEach(e -> consumer.accept(e.value));
	}
	
	@Override
	public String toString() {
		return Stream.iterate(head,  e -> e != null, e -> e.next)
			.map(e -> e.value.toString())
			.collect(Collectors.joining(", ", "[", "]"));
	}
	
	public void addFirstAll(FilteredListOld<? extends T> list) {
		list.forEach(this::addFirst);
	}
	
	public FilteredListOld<T> filtered() {
		if (head == null) {
			return new FilteredListOld<T>();
		}
		FilteredListOld<T> filteredList =  new FilteredListOld<T>();
		filteredList.head = head.filteredEntries(filter);
		return filteredList;
	}
	
	public void filteredForEach(Consumer<? super T> consumer) {
		Objects.requireNonNull(consumer);
	}
	
	@Override
	public Iterator<T> iterator() {
		return Stream.iterate(head,  e -> e != null, e -> e.next).map(e -> e.value).iterator();
	}
	
	private static class Entry<T> {
		final T value;
		final Entry<T> next;
		
		Entry(T value, Entry<T> next) {
			this.value = Objects.requireNonNull(value);
			this.next = next;
		}
		
		Entry<T> nextFiltered(Predicate<? super T> filter) {
			if (next == null) {
				return filter.test(value) ? this : null;
			}
			if (filter.test(value)) {
				return new Entry<T>(value, next.filteredEntries(filter));
			}
			return next.filteredEntries(filter);
		}
	}
}
