package fr.umlv.tpnote;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.junit.Test;

@SuppressWarnings("static-method")
public class FilteredListTest {
  // Q1
  @Test
  public void testFilteredList() {
    FilteredList<String> list = new FilteredList<>();
  }

  @Test
  public void testFilteredList2() {
    FilteredList<Integer> list = new FilteredList<>();
  }
  
  @Test
  public void testAddFirst() {
    FilteredList<String> list = new FilteredList<>();
    list.addFirst("hello");
    list.addFirst("list");
  }

  @Test
  public void testAddFirst2() {
    FilteredList<Integer> list = new FilteredList<>();
    list.addFirst(7);
    list.addFirst(42);
    list.addFirst(-5);
  }
  
  @Test(expected = NullPointerException.class)
  public void testAddFirstNull() {
    FilteredList<String> list = new FilteredList<>();
    list.addFirst(null);
  }
  
  
  // Q2
  
  interface Sink {
    void sink(String s);
  }
  
  @Test
  public void testForEach() {
    FilteredList<String> list = new FilteredList<>();
    list.addFirst("foo");
    list.addFirst("bar");
    list.addFirst("baz");
    ArrayList<String> result = new ArrayList<>();
    list.forEach(new Sink() {
      @Override
      public void sink(String s) {
        result.add(s);
      }
    }::sink);
    assertEquals(List.of("baz", "bar", "foo"), result);
  }
  
  @Test
  public void testForEachEmpty() {
    new FilteredList<>().forEach(__ -> fail());
  }
  
  @Test
  public void testForEachALot() {
    FilteredList<Integer> list = new FilteredList<>();
    for(int i = 0; i < 10_000; i++) {
      list.addFirst(i);
    }
    int[] val = { 10_000 };
    list.forEach(x -> assertEquals(--val[0], (int)x));
  }
  
  @Test(expected = NullPointerException.class)
  public void testForEachNull() {
    new FilteredList<>().forEach(null);
  }
  
  @Test
  public void testForEach2() {
    class IntConsumer implements Consumer<Number> {
      @Override
      public void accept(Number value) {
        assertTrue(value.intValue() % 2 == 0);
      }
    }
    FilteredList<Integer> list = new FilteredList<>();
    list.addFirst(4);
    list.addFirst(-56);
    list.forEach(new IntConsumer());
  }
  
  
  // Q3
  
  @Test
  public void testToString() {
    List<Integer> list = List.of(13, 566, 678, -34, 56, 78);
    ArrayList<Integer> reverse = new ArrayList<>(list);
    Collections.reverse(reverse);
    FilteredList<Integer> filteredList = new FilteredList<>();
    for(int value : reverse) {
      filteredList.addFirst(value);
    }
    assertEquals(list.toString(), filteredList.toString());
  }
  
  @Test
  public void testEmptyToString() {
    FilteredList<String> filteredList = new FilteredList<>();
    assertEquals("[]", filteredList.toString());
  }
  
  
  // Q4
  
  @Test
  public void testAddFirstAll() {
    FilteredList<String> filteredList = new FilteredList<>();
    filteredList.addFirst("foo");
    filteredList.addFirst("blizz");
    FilteredList<String> filteredList2 = new FilteredList<>();
    filteredList2.addFirst("buzz");
    filteredList2.addFirstAll(filteredList);
    assertEquals("[foo, blizz, buzz]", filteredList2.toString());
  }
  
  @Test
  public void testAddFirstAllEmpty() {
    FilteredList<String> filteredList = new FilteredList<>();
    FilteredList<Object> filteredList2 = new FilteredList<>();
    filteredList2.addFirstAll(filteredList);
    assertEquals("[]", filteredList2.toString());
  }
  
  @Test(expected = NullPointerException.class)
  public void testAddFirstAllNull() {
    FilteredList<Integer> filteredList = new FilteredList<>();
    filteredList.addFirstAll(null);
  }
  
  
  // Q5
  
  @Test
  public void testFilteredListConstructorWithFilter() {
    FilteredList<String> list = new FilteredList<>(x -> x.startsWith("foo"));
  }
  
  @Test(expected = NullPointerException.class)
  public void testFilteredListConstructorWithFilterNull() {
    new FilteredList<>(null);
  }
  
  @Test
  public void testFilteredListConstructorWithFilterSignature() {
    class IntPredicate implements Predicate<Number> {
      @Override
      public boolean test(Number number) {
        return number.intValue() == 0;
      }
    }
    FilteredList<Integer> list = new FilteredList<>(new IntPredicate());
  }
  
  @Test
  public void testFiltered() {
    FilteredList<String> list = new FilteredList<>(x -> x.endsWith("o"));
    list.addFirst("two");
    list.addFirst("one");
    list.addFirst("zero");
    FilteredList<String> filtered = list.filtered();
    assertEquals("[zero, two]", filtered.toString());
  }
  
  @Test
  public void testFilteredEmpty() {
    FilteredList<String> list = new FilteredList<>(x -> false);
    FilteredList<String> filtered = list.filtered();
    assertEquals("[]", filtered.toString());
  }
  
  @Test
  public void testFilteredMutation() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 0);
    list.addFirst(6);
    list.addFirst(4);
    list.addFirst(3);
    list.addFirst(2);
    FilteredList<Integer> filtered = list.filtered();
    filtered.addFirst(1);
    assertEquals("[1, 2, 4, 6]", filtered.toString());
    list.addFirst(0);
    assertEquals("[1, 2, 4, 6]", filtered.toString());
    assertEquals("[0, 2, 3, 4, 6]", list.toString());
  }
  
  @Test
  public void testFilteredThenForEach() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 0);
    for(int i = 0; i < 1_000; i++) {
      list.addFirst(i);
    }
    FilteredList<Integer> filtered = list.filtered();
    int[] count = { 0 };
    filtered.forEach(x -> {
      if (x % 2 == 1) {
        fail();
      }
      count[0]++;
    });
    assertEquals(500, count[0]);
  }
  
  @Test
  public void testFilteredFiltered() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 0);
    list.addFirst(4);
    list.addFirst(7);
    FilteredList<Integer> filtered = list.filtered();
    assertEquals("[4]", filtered.toString());
    assertEquals("[4]", filtered.filtered().toString());
    filtered.addFirst(1);
    assertEquals("[1, 4]", filtered.toString());
    assertEquals("[1, 4]", filtered.filtered().toString());
  }
  
  
  // Q6
  
  @Test
  public void testFilteredForEach() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 1);
    list.addFirst(6);
    list.addFirst(7);
    list.addFirst(2);
    list.addFirst(2);
    list.filteredForEach(element -> assertEquals(7, (int)element));
  }
  
  @Test
  public void testFilteredForEachALot() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 1);
    for(int i = 0; i < 1_000; i++) {
      list.addFirst(i);
    }
    list.filteredForEach(element -> {
      if (element % 2 == 0) {
        fail();
      }
    });
  }
  
  
  // Q7
  
  @Test
  public void testFilteredFilteredForEach() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 1);
    ArrayDeque<Integer> deque = new ArrayDeque<>();
    for(int i = 0; i < 1_000; i++) {
      list.addFirst(i);
      if (i % 2 == 1) {
        deque.addFirst(i);
      }
    }
    list = list.filtered();
    list.filteredForEach(element -> {
      assertEquals(deque.remove(), element);
    });
  }
  
  
  // Q8
  
  @Test
  public void testForEachIn() {
    FilteredList<Integer> list = new FilteredList<>();
    for (int i = 1_000; --i >= 0;) {
      list.addFirst(i);
    }
    int counter = 0;
    for(int value: list) {
      assertEquals(counter++, value);
    }
  }
  
  @Test
  public void testForEachInFiltered() {
    FilteredList<Integer> list = new FilteredList<>(x -> x % 2 == 0);
    for (int i = 1_000; --i >= 0;) {
      list.addFirst(i);
    }
    int counter = 0;
    for(int value: list.filtered()) {
      assertEquals(counter, value);
      counter += 2;
    }
  }

  
  // Q9
  
}