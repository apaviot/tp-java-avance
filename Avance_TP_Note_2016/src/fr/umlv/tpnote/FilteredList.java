package fr.umlv.tpnote;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilteredList<T> implements Iterable<T> {
	private Entry<T> head;
	private Entry<T> headFiltered;
	private final Predicate<? super T> filter;
	
	public FilteredList() {
		this(e -> true);	
	}
	
	public FilteredList(Predicate<? super T> filter) {
		this.filter = Objects.requireNonNull(filter);
	}
	
	public void addFirst(T value) {
		head = new Entry<T>(value, head, Entry.nextFiltered(head, filter));
		if (filter.test(value)) {
			headFiltered = head;
		}
	}
	
	public void forEach(Consumer<? super T> consumer) {
		Objects.requireNonNull(consumer);
		Stream.iterate(head,  e -> e != null, e -> e.next).forEach(e -> consumer.accept(e.value));
	}
	
	@Override
	public String toString() {
		return Stream.iterate(head,  e -> e != null, e -> e.next)
			.map(e -> e.value.toString())
			.collect(Collectors.joining(", ", "[", "]"));
	}
	
	public void addFirstAll(FilteredList<? extends T> list) {
		list.forEach(this::addFirst);
	}
	
	public FilteredList<T> filtered() {
		if (headFiltered == null) {
			return new FilteredList<T>();
		}
		FilteredList<T> filteredList =  new FilteredList<T>();
		filteredList.head = headFiltered.filteredEntries();
		filteredList.headFiltered = filteredList.head;
		return filteredList;
	}
	
	public void filteredForEach(Consumer<? super T> consumer) {
		Objects.requireNonNull(consumer);
		Stream.iterate(headFiltered, e -> e != null, e -> e.nextFiltered)
			.forEach(e -> consumer.accept(e.value));
	}
	
	@Override
	public Iterator<T> iterator() {
		return Stream.iterate(head,  e -> e != null, e -> e.next).map(e -> e.value).iterator();
	}
	
	private static class Entry<T> {
		final T value;
		final Entry<T> next;
		final Entry<T> nextFiltered;
		
		Entry(T value, Entry<T> next, Entry<T> nextFiltered) {
			this.value = Objects.requireNonNull(value);
			this.next = next;
			this.nextFiltered = nextFiltered;
		}
		
		Entry<T> filteredEntries() {
			if (nextFiltered == null) {
				return new Entry<T>(value, null, null);
			}
			return new Entry<T>(value, nextFiltered.filteredEntries(), null);
		}
		
		static <T> Entry<T> nextFiltered(Entry<T> entry, Predicate<? super T> filter) {
			Entry<T> current = entry;
			while (current != null) {
				if (filter.test(current.value)) {
					return current;
				}
				current = current.next;
			}
			return null;
		}
	}
}
