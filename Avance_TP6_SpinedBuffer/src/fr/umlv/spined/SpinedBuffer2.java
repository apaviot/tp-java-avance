package fr.umlv.spined;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A non-mutable linked list where each link contains several values in an array.
 */
public class SpinedBuffer2<E> implements Iterable<E> {

	private Object[] values;
	private final SpinedBuffer2<E> next;

	/**
	 * Creates a new link of the linked list containing several values.
	 * 
	 * @param values
	 *            the values contained in the link.
	 * @param next
	 *            a reference to the next link or null if it's the last link.
	 */
	@SafeVarargs
	private SpinedBuffer2(SpinedBuffer2<E> next, E... values) {
		this.values = Arrays.copyOf(values, values.length);
		this.next = next;
	}

	/**
	 * Returns an unmodifiable list of the values of the current link.
	 * 
	 * @return an unmodifiable list of the values of the current link.
	 */
	@SuppressWarnings("unchecked")
	List<E> getValues() {
		return (List<E>) Collections.unmodifiableList(Arrays.asList(values));
	}

	@SafeVarargs
	public static <E> SpinedBuffer2<E> of(E... values) {
		return new SpinedBuffer2<E>(null, checkValues(values));
	}

	@SuppressWarnings("unchecked")
	public SpinedBuffer2<E> prepend(E... values) {
		return new SpinedBuffer2<E>(this, checkValues(values));
	}

	@SafeVarargs
	private static <E> E[] checkValues(E... values) {
		for (E element : Objects.requireNonNull(values)) {
			Objects.requireNonNull(element);
		}
		return values;
	}

	private Stream<E> flattened() {
		return Stream.iterate(this, sb -> sb != null, sb -> sb.next).flatMap(sb -> sb.getValues().stream());
	}

	
	public void forEach(Consumer<? super E> consumer) { 
		flattened().forEach(consumer::accept); 
	}

	@Override
	public String toString() {
		return flattened().map(Object::toString).collect(Collectors.joining(" -> "));
	}

	@Override
	public Iterator<E> iterator() {
		return flattened().iterator();
	}

	@SuppressWarnings("unchecked")
	public <R> SpinedBuffer2<R> map(Function<? super E, ? extends R> fun) {
		/*
		 * List<? extends R> list = flattened().map(fun::apply).collect(Collectors.toList()); return
		 * SpinedBuffer.of((R[]) list.toArray());
		 */

		Object newValues[] = new Object[values.length];
		for (int i = 0; i < values.length; i++) {
			newValues[i] = fun.apply((E) values[i]);
		}

		if (next != null) {
			return new SpinedBuffer2<R>(next.map(fun), (R[]) newValues);
		}
		return new SpinedBuffer2<R>(null, (R[]) newValues);
	}
}