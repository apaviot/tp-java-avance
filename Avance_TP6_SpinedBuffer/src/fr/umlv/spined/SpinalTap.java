package fr.umlv.spined;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SpinalTap<E> implements Iterable<E> {
	
	private Stock<?, E> stock;
	private final SpinalTap<?> next;

	/**
	 * Creates a new link of the linked list containing several values.
	 * 
	 * @param values
	 *            the values contained in the link.
	 * @param spinalTap
	 *            a reference to the next link or null if it's the last link.
	 */
	private SpinalTap(SpinalTap<?> spinalTap, Stock<?, E> stock) {
		this.stock = Objects.requireNonNull(stock);
		this.next = spinalTap;
	}

	/**
	 * Returns an unmodifiable list of the values of the current link.
	 * 
	 * @return an unmodifiable list of the values of the current link.
	 */
	@SuppressWarnings("unchecked")
	List<E> getValues() {
		return (List<E>) Collections.unmodifiableList(stock.getValues());
	}

	@SafeVarargs
	public static <E> SpinalTap<E> of(E... values) {
		return new SpinalTap<E>(null, new Values<E>(values));
	}

	@SuppressWarnings("unchecked")
	public SpinalTap<E> prepend(E... values) {
		return new SpinalTap<E>(this, new Values<E>(values));
	}

	private Stream<E> flattened() {
		return Stream.iterate(this, sb -> sb != null, sb -> sb.next).flatMap(sb -> sb.getValues().stream());
	}
	
	// Faster than default forEach method ?
	public void forEach(Consumer<? super E> consumer) {
		flattened().forEach(consumer::accept);
	}

	@Override
	public String toString() {
		return flattened().map(Object::toString).collect(Collectors.joining(" -> "));
	}

	@Override
	public Iterator<E> iterator() {
		return flattened().iterator();
	}
	
	@SuppressWarnings("unchecked")
	public <R> SpinalTap<R> map(Function<? super E, ? extends R> fun) {
		new Mapper<R>(fun);
		return new SpinalTap<R>(this, new Mapper<R>(fun));
	}
	
	
	
	
	private interface Stock<E, R> {
		List<R> getValues();
	}
	
	private static class Values<T> implements Stock<T, T> {
		Object elements[];
		
		@SafeVarargs
		Values(T... values) {
			elements = Arrays.copyOf(checkValues(values), values.length);
		}
		
		private T[] checkValues(@SuppressWarnings("unchecked") T... values) {
			for (T element : Objects.requireNonNull(values)) {
				Objects.requireNonNull(element);
			}
			return values;
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<T> getValues() {
			return (List<T>) Collections.unmodifiableList(Arrays.asList(elements));
		}
	}
	
	private class Mapper<R> implements Stock<E, R> {
		Function<? super E, ? extends R> mappingFunction;
		
		Mapper(Function<? super E, ? extends R> mappingFunction) {
			this.mappingFunction = mappingFunction;
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<R> getValues() {
			return (List<R>) Collections.unmodifiableCollection(
				next.getValues().stream().map(e -> mappingFunction.apply(e)).collect(Collectors.toList())
			);
		}
		
	}
	
}
