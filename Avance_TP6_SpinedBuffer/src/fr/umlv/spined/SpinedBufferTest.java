package fr.umlv.spined; 

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;

import org.junit.Test;

public class SpinedBufferTest {
  // Q2
  @Test
  public void testOf() {
    assertEquals(List.of(1), SpinedBuffer.of(1).getValues());
    assertEquals(List.of(7, 13, 45), SpinedBuffer.of(7, 13, 45).getValues());
  }

  @Test(expected = NullPointerException.class)
  public void testOfNull() {
    SpinedBuffer.of((Object[])null);
  }
  @Test(expected = NullPointerException.class)
  public void testOfNull2() {
    SpinedBuffer.of(null, "foo");
  }
  @Test(expected = NullPointerException.class)
  public void testOfNull3() {
    SpinedBuffer.of("bar", null);
  }
  
  @Test
  public void testOfImmutable() {
    String[] array = new String[] { "foo" };
    SpinedBuffer<String> seq = SpinedBuffer.of(array);
    array[0] = "bar";
    assertEquals("foo", seq.getValues().get(0));
  }
  
  @Test
  public void testEmpty() {
    assertEquals(List.of(), SpinedBuffer.of().getValues());
  }
  
  @Test
  public void testOneConstructor() {
    Constructor<?>[] constructors = SpinedBuffer.class.getDeclaredConstructors();
    assertEquals(1, constructors.length);
    assertTrue(Modifier.isPrivate(constructors[0].getModifiers()));
  }
  
  @Test
  public void testPrepend() {
    SpinedBuffer<String> seq = SpinedBuffer.of("world");
    seq = seq.prepend("hello");
    assertEquals(List.of("hello"), seq.getValues());
  }
  
  @Test(expected = NullPointerException.class)
  public void testPrependNull() {
    SpinedBuffer.of("wizz").prepend((String[])null);
  }
  @Test(expected = NullPointerException.class)
  public void testPrependNull2() {
    SpinedBuffer.of("wazz").prepend("fuzz", null);
  }
  @Test(expected = NullPointerException.class)
  public void testPrependNull3() {
    SpinedBuffer.of("wuzz").prepend(null, "woosh");
  }
  
  @Test
  public void testPrependImmutable() {
    String[] array = new String[] { "foo" };
    SpinedBuffer<String> seq = SpinedBuffer.of("").prepend(array);
    array[0] = "bar";
    assertEquals("foo", seq.getValues().get(0));
  }
  
  // Q3
  @Test
  public void testForEach() {
    SpinedBuffer<String> seq = SpinedBuffer.of("a", "b", "c");
    ArrayList<String> list = new ArrayList<>();
    seq.forEach(list::add);
    assertEquals(List.of("a", "b", "c"), list);
  }
  
  @Test
  public void testForEach2() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(56, 98);
    seq = seq.prepend(-4, 34);
    ArrayList<Integer> list = new ArrayList<>();
    seq.forEach(list::add);
    assertEquals(List.of(-4, 34, 56, 98), list);
  }
  
  @Test
  public void testForEachEmpty() {
    SpinedBuffer.of().forEach(__ -> fail());
  }
  
  @Test
  public void testForEachContravariance() {
    Consumer<Object> consumer = __ -> {
      // empty
    };
    SpinedBuffer.of(1, 2, 3).forEach(consumer);
  }

  // Q4
  @Test
  public void testForEachBig() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of();
    for(int i = 0; i < 1_000_000; i++) {
      seq = seq.prepend(i);
    }
    long[] sum = new long[1];
    seq.forEach(v -> sum[0] += v);
    assertEquals(1_000_000L * (1_000_000 - 1) / 2, sum[0]);
  }
  
  // Q5
  @Test
  public void testToString() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(1, 3, 6, 9);
    assertEquals("1 -> 3 -> 6 -> 9", seq.toString());
  }
  
  @Test
  public void testToString2() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(6, 9).prepend(1, 3);
    assertEquals("1 -> 3 -> 6 -> 9", seq.toString());
  }
  
  @Test
  public void testToStringEmpty() {
    SpinedBuffer<String> seq = SpinedBuffer.of();
    assertEquals("", seq.toString());
  }

  // Q6
  @Test
  public void testIterator() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(45, 78);
    Iterator<Integer> it = seq.iterator();
    assertTrue(it.hasNext());
    assertEquals(45, (int)it.next());
    assertTrue(it.hasNext());
    assertEquals(78, (int)it.next());
    assertFalse(it.hasNext());
  }

  @Test
  public void testIterator2() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(0, -1);
    Iterator<Integer> it = seq.iterator();
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(0, (int)it.next());
    assertEquals(-1, (int)it.next());
    assertFalse(it.hasNext());
  }
  
  @Test
  public void testIterator3() {
    SpinedBuffer<String> seq = SpinedBuffer.of("+", "++").prepend("+++");
    Iterator<String> it = seq.iterator();
    assertTrue(it.hasNext());
    assertEquals("+++", it.next());
    assertTrue(it.hasNext());
    assertEquals("+", it.next());
    assertTrue(it.hasNext());
    assertEquals("++", it.next());
    assertFalse(it.hasNext());
  }
  
  @Test
  public void testIterator4() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(2, 3).prepend(0, 1);
    Iterator<Integer> it = seq.iterator();
    it.next();
    it.next();
    it.next();
    it.next();
    try {
      it.next();
      fail();
    } catch(NoSuchElementException e) {
      // ok
    }
  }
  
  @Test
  public void testIteratorEmpty() {
    SpinedBuffer<Double> seq = SpinedBuffer.of();
    Iterator<Double> it = seq.iterator();
    assertFalse(it.hasNext());
  }
  
  @Test
  public void testIteratorEmptyInMiddle() {
    SpinedBuffer<String> seq = SpinedBuffer.of("2").prepend().prepend("1");
    Iterator<String> it = seq.iterator();
    assertTrue(it.hasNext());
    assertEquals("1", it.next());
    assertTrue(it.hasNext());
    assertEquals("2", it.next());
    assertFalse(it.hasNext());
  }
  
  @Test(expected = NoSuchElementException.class)
  public void testIteratorEmpty2() {
    SpinedBuffer<Double> seq = SpinedBuffer.of();
    seq.iterator().next();
  }
  
  @Test(expected = UnsupportedOperationException.class)
  public void testIteratorRemove() {
    SpinedBuffer<Double> seq = SpinedBuffer.of(1.0);
    Iterator<Double> it = seq.iterator();
    it.next();
    it.remove();
  }

  @Test
  public void testIteratorBig() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of();
    for(int i = 0; i < 1_000_000; i++) {
      seq = seq.prepend(i);
    }
    Iterator<Integer> it = seq.iterator();
    for(int i = 1_000_000; --i >= 0;) {
      assertEquals(i, (int)it.next());
    }
  }
  
  // Q7
  @Test
  public void testForeachLoop() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(1, 2, 5);
    ArrayList<Integer> list = new ArrayList<>();
    for(int value: seq) {
      list.add(value);
    }
    assertEquals(List.of(1, 2, 5), list);
  }
  
  // Q8
  @Test
  public void testMap() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(5, 8, 12);
    seq = seq.map(x -> x * 2);
    assertEquals(List.of(10, 16, 24), seq.getValues());
  }
  
  
  @Test
  public void testMap2() {
    SpinedBuffer<String> seq = SpinedBuffer.of("bar", "bazz");
    SpinedBuffer<Integer> seq2 = seq.map(String::length);
    seq2 = seq2.prepend(1, 2);
    ArrayList<Integer> list = new ArrayList<>();
    seq2.forEach(list::add);
    assertEquals(List.of(1, 2, 3, 4), list);
  }
  
  @Test
  public void testMap3() {
    SpinedBuffer<Integer> seq = SpinedBuffer.of(4, 5).prepend(1, 2, 3);
    SpinedBuffer<String> seq2 = seq.map(Object::toString);
    ArrayList<String> list = new ArrayList<>();
    seq2.forEach(list::add);
    assertEquals(List.of("1", "2", "3", "4", "5"), list);
  }
  
  @Test
  public void testMapMap() {
    SpinedBuffer<String> seq = SpinedBuffer.of("1357", "2468");
    SpinedBuffer<Integer> map = seq.map(Integer::parseInt);
    SpinedBuffer<String> map2 = map.map(Object::toString);
    ArrayList<String> list = new ArrayList<>();
    map2.forEach(list::add);
    assertEquals(List.of("1357", "2468"), list);
  }
  
  @Test
  public void testMapCovariance() {
    Function<String, String> fun = x -> x;
    SpinedBuffer<Object> seq = SpinedBuffer.of("foo", "bar").map(fun);
    assertEquals(List.of("foo", "bar"), seq.getValues());
  }
  
  @Test
  public void testMapCovariance2() {
    Function<Object, String> fun = Object::toString;
    SpinedBuffer<String> seq = SpinedBuffer.of("foo", "bar").map(fun);
    assertEquals(List.of("foo", "bar"), seq.getValues());
  }
}