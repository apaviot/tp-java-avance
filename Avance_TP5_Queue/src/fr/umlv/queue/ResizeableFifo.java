package fr.umlv.queue;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class ResizeableFifo<E> extends AbstractQueue<E> {
	
	private Object elements[];
	private int head;
	private int queue;
	private int size;

	public ResizeableFifo(int capacity) {
		elements = new Object[requirePositive(capacity)];
		head = 0;
		queue = 0;
		size = 0;
	}
	
	private int requirePositive(int value) {
		if (value > 0) {
			return value;
		}
		throw new IllegalArgumentException("The value must be positive : " + value);
	}
	
	@Override
	public boolean offer(E element) {
		if (size == elements.length) {
			doubleCapacity();
		}
		elements[queue] = Objects.requireNonNull(element);
		queue = (queue + 1) % elements.length;
		size++;
		return true;
	}
	
	private void doubleCapacity() {
		int capacity = elements.length;
		Object array[] = new Object[capacity * 2];
		System.arraycopy(elements, head, array, 0, capacity - head);
		System.arraycopy(elements, 0, array, capacity - head, head);
		elements = array;
		head = 0;
		queue = size;
		
		/*
		int capacity = elements.length;
		Object array[] = new Object[capacity * 2];
		for (int i = 0; i < size; i++) {
			array[i] = elements[(head + i) % capacity];
		}
		head = 0;
		queue = size;
		elements = array;
		*/
	}

	@Override
	@SuppressWarnings("unchecked")
	public E peek() {
		return (E) elements[head];
	}

	@Override
	@SuppressWarnings("unchecked")
	public E poll() {
		Object tmp = elements[head];
		elements[head] = null;
		head = (head + 1) % elements.length;
		size--;
		return (E) tmp;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			
			private int index = head;
			private int start = head;
			private int size = size();

			@Override
			public boolean hasNext() {
				return index < start + size;
			}

			@Override
			@SuppressWarnings("unchecked")
			public E next() {
				if (hasNext()) {
					Object tmp = elements[index % elements.length];
					index++;
					return (E) tmp;
				}
				throw new NoSuchElementException();
			}
		};
	}

	@Override
	public int size() {
		return size;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int capacity = elements.length;
		if (size > 0) {
			sb.append(elements[head]);
		}
		for(int i = head + 1; i < head + size; i++) {
			sb.append(", ").append(elements[i % capacity]);
		}
		sb.append("]");
		return sb.toString();
	}

}
