package fr.umlv.queue;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class Fifo<E> implements Iterable<E> {

	private final Object elements[];
	private int head;
	private int queue;
	private int size;
	
	public Fifo(int capacity) {
		elements = new Object[requirePositive(capacity)];
		head = 0;
		queue = 0;
		size = 0;
	}
	
	private int requirePositive(int value) {
		if (value > 0)
			return value;
		throw new IllegalArgumentException("The value must be positive : " + value);
	}
	
	public void offer(E element) {
		if (size == elements.length) {
			throw new IllegalStateException("The queue is full");
		}
		elements[queue] = Objects.requireNonNull(element);
		queue = (queue + 1) % elements.length;
		size++;
	}
	
	@SuppressWarnings("unchecked")
	public E poll() {
		if (size == 0) {
			throw new IllegalStateException("The queue is empty");
		}
		Object tmp = elements[head];
		elements[head] = null;
		head = (head + 1) % elements.length;
		size--;
		return (E) tmp;
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public int size() {
		return size;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		int capacity = elements.length;
		if (size > 0) {
			sb.append(elements[head]);
		}
		for(int i = head + 1; i < head + size; i++) {
			sb.append(", ").append(elements[i % capacity]);
		}
		sb.append("]");
		return sb.toString();
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			
			private int index = head;
			private int start = head;
			private int size = size();

			@Override
			public boolean hasNext() {
				return index < start + size;
			}

			@Override
			@SuppressWarnings("unchecked")
			public E next() {
				if (hasNext()) {
					Object tmp = elements[index % elements.length];
					index++;
					return (E) tmp;
				}
				throw new NoSuchElementException();
			}
		};
	}
	
	
}
