package fr.umlv.queue;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;
 
@SuppressWarnings("static-method")
public class FifoTest {
  @Test(expected=IllegalArgumentException.class)
  public void testFifoCapacity() {
    new Fifo<Object>(-3);
  }   
  
  @Test(expected=IllegalArgumentException.class)
  public void testFifoCapacity2() {
    new Fifo<Object>(0);
  }

  @Test(expected=IllegalStateException.class)
  public void testEmpty() {
    Fifo<Object> fifo = new Fifo<>(1);
    fifo.poll();
  }
  
  
  @Test(expected=IllegalStateException.class)
  public void testFull() {
    Fifo<Integer> fifo = new Fifo<>(1);
    fifo.offer(43);
    fifo.offer(7);
  }

  @Test(expected=NullPointerException.class)
  public void testOfferNull() {
    Fifo<Object> fifo = new Fifo<>(234);
    fifo.offer(null);
  }
  
  @Test
  public void testOfferPoll() {
    Fifo<Integer> fifo = new Fifo<>(2);
    fifo.offer(9);
    assertEquals(9, (int)fifo.poll());
    fifo.offer(2);
    fifo.offer(37);
    assertEquals(2, (int)fifo.poll());
    fifo.offer(12);
    assertEquals(37, (int)fifo.poll());
    assertEquals(12, (int)fifo.poll());
  }
  
  @Test
  public void testFullToEmpty() {
    Fifo<Object> fifo = new Fifo<>(20);
    for(int i = 0; i < 20; i++) {
      fifo.offer(i);
    }
    assertEquals(0, (int)fifo.poll());
    fifo.offer("foo");
    for(int i = 1; i < 20; i++) {
      assertEquals(i, (int)fifo.poll());
    }
    assertEquals("foo", fifo.poll());
  }

  @Test
  public void testSize() {
    Fifo<String> fifo = new Fifo<>(2);
    assertEquals(0, fifo.size());
    fifo.offer("foo");
    assertEquals(1, fifo.size());
    fifo.offer("bar");
    assertEquals(2, fifo.size());
    fifo.poll();
    assertEquals(1, fifo.size());
    fifo.poll();
    assertEquals(0, fifo.size());
  }
  
  @Test
  public void testSizeEmpty() {
    Fifo<Object> fifo = new Fifo<>(1);
    assertEquals(0, fifo.size());
  }
  
  @Test
  public void testSizeFull() {
    Fifo<String> fifo = new Fifo<>(1);
    fifo.offer("dooh");
    assertEquals(1, fifo.size());
  }
  
  @Test
  public void testIsEmpty() {
    Fifo<String> fifo = new Fifo<>(2);
    assertTrue(fifo.isEmpty());
    fifo.offer("oof");
    assertFalse(fifo.isEmpty());
    fifo.offer("rab");
    assertFalse(fifo.isEmpty());
    fifo.poll();
    fifo.poll();
    assertTrue(fifo.isEmpty());
  }
 
  
  @Test
  public void testEmptyToString() {
    Fifo<Object> fifo = new Fifo<>(23);
    assertEquals("[]", fifo.toString());
  }
  
  @Test
  public void testOneElementToString() {
    Fifo<String> fifo = new Fifo<>(23);
    fifo.offer("joe");
    assertEquals("[joe]", fifo.toString());
  }
  
  @Test
  public void testTwoElementToString() {
    Fifo<String> fifo = new Fifo<>(23);
    fifo.offer("jane");
    fifo.offer("doe");
    assertEquals("[jane, doe]", fifo.toString());
  }
  
  @Test
  public void testCircularToString() {
    Fifo<String> fifo = new Fifo<>(2);
    fifo.offer("foo");
    fifo.poll();
    fifo.offer("1");
    fifo.offer("2");
    assertEquals("[1, 2]", fifo.toString());
  }
  
  @Test
  public void testNonMutateToString() {
    Fifo<Integer> fifo = new Fifo<>(200);
    ArrayList<Integer> list = new ArrayList<>();
    for(int i = 0; i < 100; i++) {
      fifo.offer(i);
      list.add(i);
    }
    assertEquals(list.toString(), fifo.toString());
    for(int i = 0; i < 100; i++) {
      assertEquals(i, (int)fifo.poll());
    }
  }
  
  @Test
  public void testFullToString() {
    Fifo<Integer> fifo = new Fifo<>(99);
    ArrayList<Integer> list = new ArrayList<>();
    for(int i = 0; i < 99; i++) {
      fifo.offer(i);
      list.add(i);
    }
    assertEquals(list.toString(), fifo.toString());
  }
  
 
  
  @Test
  public void testIteratorSignature() {
    Fifo<String> fifo = new Fifo<>(1);
    Iterator<String> it = fifo.iterator();
    assertNotNull(it);
  }
  
  @Test(expected = NoSuchElementException.class)
  public void testIteratorEmpty() {
    Fifo<String> fifo = new Fifo<>(1);
    fifo.offer("bar");
    fifo.poll();
    Iterator<String> it = fifo.iterator();
    it.next();
  }
  
  @Test
  public void testIterator() {
    Fifo<Integer> fifo = new Fifo<>(3);
    fifo.offer(117);
    fifo.offer(440);
    Iterator<Integer> it = fifo.iterator();
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(117, (int)it.next());
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(440, (int)it.next());
    assertFalse(it.hasNext());
    assertFalse(it.hasNext());
  }
   
  @Test
  public void testIteratorCircular() {
    Fifo<Integer> fifo = new Fifo<>(2);
    fifo.offer(42);
    fifo.poll();
    fifo.offer(55);
    fifo.offer(333);
    Iterator<Integer> it = fifo.iterator();
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(55, (int)it.next());
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(333, (int)it.next());
    assertFalse(it.hasNext());
    assertFalse(it.hasNext());
  }
  
  @Test
  public void testTwoIterators() {
    Fifo<Integer> fifo = new Fifo<>(1);
    fifo.offer(898);
    
    Iterator<Integer> it = fifo.iterator();
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertEquals(898, (int)it.next());
    assertFalse(it.hasNext());
    assertFalse(it.hasNext());
    Iterator<Integer> it2 = fifo.iterator();
    assertTrue(it2.hasNext());
    assertTrue(it2.hasNext());
    assertEquals(898, (int)it2.next());
    assertFalse(it2.hasNext());
    assertFalse(it2.hasNext());
  }
  
  @Test
  public void testIterator3FalseHasNext() {
    Fifo<Object> fifo = new Fifo<>(1);
    Iterator<Object> it = fifo.iterator();
    assertFalse(it.hasNext());
    assertFalse(it.hasNext());
    assertFalse(it.hasNext());
  }
  
  @Test
  public void testIterator3TrueHasNext() {
    Fifo<Object> fifo = new Fifo<>(1);
    fifo.offer("one");
    Iterator<Object> it = fifo.iterator();
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
    assertTrue(it.hasNext());
  }
  
  
  
  @Test
  public void testIteratorALot() {
    Fifo<Integer> fifo = new Fifo<>(10_000);
    for(int i=0; i<10_000; i++) {
      fifo.offer(i);
    }
    int i = 0;
    Iterator<Integer> it = fifo.iterator();
    while(it.hasNext()) {
      assertEquals(i++, (int)it.next());
    }
    assertEquals(10000, fifo.size());
  }
  
  @Test(expected=UnsupportedOperationException.class)
  public void testIteratorRemove() {
    Fifo<String> fifo = new Fifo<>(1);
    fifo.offer("foooo");
    fifo.iterator().remove();
  }
  
  // this test needs a lot of memory (more than 8 giga)
  // so it is disable by default
  // use the option -Xmx9g when running the VM
  @Test
  public void testIteratorOverflow() {
    Fifo<Integer> fifo = new Fifo<>(Integer.MAX_VALUE - 8);
    for(int i = 0; i < Integer.MAX_VALUE / 2; i++) {
      fifo.offer(i % 100);
      fifo.poll();
    }
    for(int i = 0; i < Integer.MAX_VALUE - 8; i++) {
      fifo.offer(i % 100);
    }
    int counter = 0;
    for(Iterator<Integer> it = fifo.iterator(); it.hasNext(); counter = (counter + 1) % 100) {
      assertEquals(counter, (int)it.next());
    }
  }
  
  @Test
  public void testIterable() {
    Fifo<Integer> fifo = new Fifo<>(100);
    fifo.offer(222);
    fifo.poll();
    
    for(int i=0; i<100; i++) {
      fifo.offer(i);
    }
    int i = 0; 
    for(int value: fifo) {
      assertEquals(i++, value);
    }
    assertEquals(100, fifo.size());
  }
}