package fr.umlv.lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Lambda {

	public static long count(List<String> words, String word) {
		long counter = 0;
		for (String w : words) {
			if (w.equals(word)) {
				counter++;
			}
		}
		return counter;
	}

	public static long count2(List<String> words, String word) {
		return words.stream().filter(w -> w.equals(word)).count();
	}
	
	public static int count3(List<String> words, String word) {
		//return words.stream().filter(word::equals).mapToInt(w -> 1).sum();
		return words.stream().filter(word::equals).mapToInt(w -> 1).reduce(0, (i, result) -> result += i);
	}

	public static List<String> upperCase(List<String> words) {
		List<String> list = new ArrayList<>();
		for (String w : words) {
			list.add(w.toUpperCase());
		}
		return list;
	}

	public static List<String> upperCase2(List<String> words) {
		List<String> list = new ArrayList<>();
		words.stream().map(w -> w.toUpperCase()).forEach(w -> list.add(w));
		return list;
	}

	public static List<String> upperCase3(List<String> words) {
		List<String> list = new ArrayList<>();
		words.stream().map(String::toUpperCase).forEach(list::add);
		return list;
	}

	public static List<String> upperCase4(List<String> words) {
		return words.stream().map(String::toUpperCase).collect(Collectors.toList());
	}
	
	public static void time(Supplier<?> method) {
		long start = System.nanoTime();
		method.get();
		long end = System.nanoTime();
		System.out.println("elapsed time " + (end - start));
	}

	public static void printAndTime(LongSupplier method) {
		long start = System.nanoTime();
		long result = method.getAsLong();
		long end = System.nanoTime();
		System.out.println("result " + result);
		System.out.println(" elapsed time " + (end - start));
	}

	public static void main(String[] args) {
		List<String> list2 = new Random(0)
			.ints(10_000_000, 0, 100)
			.mapToObj(Integer::toString)
			.collect(Collectors.toList());
		
		System.out.println("---------- COUNT ----------");
		printAndTime(() -> count(list2, "33"));
		printAndTime(() -> count2(list2, "33"));
		printAndTime(() -> count3(list2, "33"));
		
		System.out.println("\n---------- UPPERCASE ----------");
		time(() -> upperCase(list2));
		time(() -> upperCase2(list2));
		time(() -> upperCase3(list2));
		time(() -> upperCase4(list2));
	}
	
	/* ---------- Exercice 1 ----------
	 * 
	 * 2. Pour obtenir un stream à partir d'un objet de type List on utilise la méthode d'instance stream().
	 *    
	 *    Les méthodes permettant de filtrer un stream et de compter son nombre d'éléments sont respectivement
	 *    filter(Predicate<? super T>) et count(). T correspond aux types des éléments à filtrer.
	 * 
	 * 
	 * ---------- Exercice 2 ----------
	 * 
	 * 2. On peut utiliser la méthode Stream.map pour remplacer toutes les chaînes de caractères par leur version en majuscule.
	 * 
	 * 3. Le premier "::" fait référence à une méthode de classe tandis que le second fait référence à une méthode d'instance.
	 *
	 *
	 * ---------- Exercice 3 ----------
	 * 
	 * 1. La variable locale list2 contient une liste de 1 000 000 de String représentant des entiers compris entre 0 et 100.
	 * 
	 * 3. Pour ne pas duplier le code pour le calcul du temps d'exécution on peut donner une la fonction de calcul à exécuter
	 *    en paramètre à la fonction printAndTime. Pour cela on va utiliser l'interface fonctionnalle LongSupplier.
	 *    
	 * 4. On peut voir que les temps d'éxécution sont assez semblables pour les stream et les boucles classiques. Parfois
	 *    les boucles sont plus rapides, parfois ce sont les streams qui sont plus rapides. Par conséquent il est préférable
	 *    d'utiliser les streams car le code est plus lisible et donc plus facilement maintenable.
	 *    
	 *    
	 * ---------- Exercice 4 ----------
	 * 
	 * 1. Nous allons utiliser la méthode mapToInt pour éviter de faire du boxing pour tous les éléments.
	 * 
	 * 3. Après plusieurs exécutions on peut voir que le temps d'exécution de count3 est sensiblement le même que count1
	 *    et count2.
	 */
}
