package set;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class IntHashSet {
	private final int DEFAULT_SIZE = 2;
	
	private final Entry entries[];
	private int size;
	
	public IntHashSet() {
		entries = new Entry[DEFAULT_SIZE];
		size = 0;
	}

	public void add(int element) {
		int hash = computeHashCode(element);
		if (entries[hash] == null) {
			entries[hash] = new Entry(element, null);
			size++;
		}
		else if (!entries[hash].contains(element)) {
			entries[hash] = new Entry(element, entries[hash]);
			size++;
		}
	}
	
	private int computeHashCode(int element) {
		int hash = 0;
		int powOfTwo = 1;
		for (int i = 0; i < entries.length / 2; i++) {
			hash += (element & 1) * powOfTwo;
			element = element >>> 1;
			powOfTwo *= 2;
		}
		return hash;
	}

	public boolean contains(int element) {
		int hash = computeHashCode((int) element);
		if (entries[hash] == null) {
			return false;
		}
		return entries[hash].contains((int) element);
	}

	public int size() {
		return size;
	}

	public void forEach(Consumer<Integer> method) {
		Arrays.stream(entries)
			.filter(e -> e != null)
			.flatMap(e -> Stream.iterate(e, i -> i != null, i -> i.next))
			.mapToInt(entry -> entry.value)
			.forEach(method::accept);
	}
	
	static private class Entry {

		final int value;
		Entry next;
		
		Entry(int value, Entry next) {
			this.value = value;
			this.next = next;
		}
		
		boolean contains(int value) {
			Entry current = this;
			while (current != null) {
				if (current.value == value) {
					return true;
				}
				current = current.next;
			}
			return false;
		}
	}
}
