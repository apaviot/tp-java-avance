package set;

import java.util.function.Consumer;

public interface HashSet<E> {

	public boolean add(E element);
	
	public boolean contains(Object element);
	
	public int size();
	
	public void forEach(Consumer<? super E> method);
	
	static class Entry<E> {

		final E value;
		Entry<E> next;
		
		Entry(E value, Entry<E> next) {
			this.value = value;
			this.next = next;
		}
		
		boolean contains(Object value) {
			Entry<E> current = this;
			while (current != null) {
				if (current.value.equals(value)) {
					return true;
				}
				current = current.next;
			}
			return false;
		}
	}
}
