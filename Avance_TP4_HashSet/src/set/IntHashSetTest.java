package set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.IntStream;

import org.junit.Test;

public class IntHashSetTest {
  @Test
  public void testAddOdd() {
    IntHashSet set = new IntHashSet();
    IntStream.range(0, 100).map(i -> i * 2 + 1).forEach(set::add);
  }
  
  @Test
  public void testAddEven() {
    IntHashSet set = new IntHashSet();
    IntStream.range(0, 100).map(i -> i * 2).forEach(set::add);
  }
  
  @Test
  public void testSizeEmpty() {
    IntHashSet set = new IntHashSet();
    assertEquals(0, set.size());
  }
  
  @Test
  public void testAddAndSize() {
    IntHashSet set = new IntHashSet();
    set.add(3);
    assertEquals(1, set.size());
    set.add(-777);
    assertEquals(2, set.size());
    set.add(3);
    assertEquals(2, set.size());
    set.add(-777);
    assertEquals(2, set.size());
  }

  @Test
  public void testForEachEmpy() {
    IntHashSet set = new IntHashSet();
    set.forEach(__ -> fail());
  }
  
  @Test
  public void testForEachSum() {
    int length = 100;
    IntHashSet set = new IntHashSet();
    IntStream.range(0, length).forEach(set::add);
    int[] sum = { 0 };
    set.forEach(value -> sum[0] += value);
    assertEquals(length * (length - 1) / 2, sum[0]);
  }
  
  @Test
  public void testForEachSet() {
    IntHashSet set = new IntHashSet();
    IntStream.range(0, 100).forEach(set::add);
    HashSet<Integer> hashSet = new HashSet<>();
    set.forEach(hashSet::add);
    assertEquals(set.size(), hashSet.size());
  }
  
  @Test
  public void testForEachSet2() {
    IntHashSet set = new IntHashSet();
    IntStream.range(0, 100).forEach(set::add);
    ArrayList<Integer> list = new ArrayList<>();
    set.forEach(list::add);
    list.sort(null);
    IntStream.range(0, 100).forEach(i -> assertEquals(i, (int)list.get(i)));
  }
  
  @Test
  public void testContains() {
    IntHashSet set = new IntHashSet();
    assertFalse(set.contains(4));
    assertFalse(set.contains(7));
    assertFalse(set.contains(1));
    assertFalse(set.contains(0));
  }
  
  @Test
  public void testAddContains() {
    IntHashSet set = new IntHashSet();
    for(int i = 0; i < 10; i++) {
      assertFalse(set.contains(i));
      set.add(i);
      assertTrue(set.contains(i));
    }
  }
  
  @Test
  public void testAddTwiceContains() {
    IntHashSet set = new IntHashSet();
    assertFalse(set.contains(Integer.MIN_VALUE));
    set.add(Integer.MIN_VALUE);
    assertTrue(set.contains(Integer.MIN_VALUE));
    set.add(Integer.MIN_VALUE);
    assertTrue(set.contains(Integer.MIN_VALUE));
  }
}