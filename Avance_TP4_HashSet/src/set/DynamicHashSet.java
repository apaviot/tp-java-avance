package set;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class DynamicHashSet<E> implements HashSet<E> {
	
	private static final int INITIAL_CAPACITY = 8;
	
	private Object entries[];
	private int capacity;
	private int size;
	
	public DynamicHashSet() {
		capacity = INITIAL_CAPACITY;
		size = 0;
		entries = new Object[capacity];
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public boolean add(E element) {
		int hash = computeHashCode(element);
		if (entries[hash] == null) {
			entries[hash] = new Entry<E>(element, null);
			size++;
			adjustCapacity();
			return true;
		}
		else if (!((Entry<E>)entries[hash]).contains(element)) {
			entries[hash] = new Entry<E>(element, (Entry<E>)entries[hash]);
			size++;
			adjustCapacity();
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private void addToArray(Object entries[], E element) {
		int hash = computeHashCode(element);
		if (entries[hash] == null) {
			entries[hash] = new Entry<E>(element, null);
		} else {
			entries[hash] = new Entry<E>(element, ((Entry<E>)entries[hash]));
		}
	}
	
	private void adjustCapacity() {
		if (size > capacity / 2) {
			capacity *= 2;
			final Object new_entries[] = new Object[capacity];
			forEach(el -> addToArray(new_entries, el));
			entries = new_entries;
		}
	}
	
	private int computeHashCode(Object element) {
		return element.hashCode() & capacity - 1;
	}

	@Override
	@SuppressWarnings("unchecked")
	public boolean contains(Object element) {
		int hash = computeHashCode(element);
		if (entries[hash] == null)
			return false;
		return ((Entry<E>) entries[hash]).contains((int) element);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void forEach(Consumer<? super E> method) {
		Arrays.stream(entries)
			.filter(e -> e != null)
			.flatMap(e -> Stream.iterate(e, i -> i != null, i -> ((Entry<E>)i).next))
			.map(entry -> ((Entry<E>)entry).value)
			.forEach(method::accept);
	}
	
	public void addAll(Collection<? extends E> elements) {
		elements.forEach(this::add);
	}
}
