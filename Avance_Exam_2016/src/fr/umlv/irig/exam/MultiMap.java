package fr.umlv.irig.exam;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;

public interface MultiMap<K, V> {
	void add(K key, V value);
	Set<V> getValuesForKey(Object key);
	int size();
	void forEach(BiConsumer<? super K, ? super V> consumer);
	void addAll(MultiMap<? extends K, ? extends V> map);
	Collection<V> allValues();
	Map<K, V> asMapOfLastValues();
	
	static <K, V> MultiMap<K, V> create() {
		return new MultiMapMap<K, V>();
	}
	
	@SafeVarargs
	static <K, V> MultiMap<K, V> create(Map.Entry<? extends K, ? extends V> ... entries) {
		Objects.requireNonNull(entries);
		MultiMap<K, V> map = create();
		for (Map.Entry<? extends K, ? extends V> e : entries) {
			Objects.requireNonNull(e);
			map.add(Objects.requireNonNull(e.getKey()), Objects.requireNonNull(e.getValue()));
		}
		return map;
	}
}
