package fr.umlv.irig.exam;

import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

class MultiMapMap<K, V> implements MultiMap<K, V> {
	private final LinkedHashMap<K, LinkedHashSet<V>> values;
	private int size;
	
	MultiMapMap() {
		values = new LinkedHashMap<>();
	}

	@Override
	public void add(K key, V value) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(value);
		if (values.computeIfAbsent(key, __ -> new LinkedHashSet<V>()).add(value)) {
			size++;
		}
	}

	@Override
	public Set<V> getValuesForKey(Object key) {
		Objects.requireNonNull(key);
		return Collections.unmodifiableSet(values.getOrDefault(key, new LinkedHashSet<V>()));
	}

	@Override
	public int size() {
		return size;
	}
	
	@Override
	public String toString() {
		return values.entrySet().stream()
			.map(e -> entryToString(e))
			.collect(Collectors.joining(", ", "{", "}"));
	}
	
	private String entryToString(Entry<K, LinkedHashSet<V>> entry) {
		String key = entry.getKey().toString();
		return entry.getValue().stream().map(v -> key + ": " + v).collect(Collectors.joining(", "));
	}

	@Override
	public void forEach(BiConsumer<? super K, ? super V> consumer) {
		Objects.requireNonNull(consumer);
		values.entrySet().stream().forEach(e -> {
			K key = e.getKey();
			for (V value : e.getValue()) {
				consumer.accept(key, value);
			}
		});
	}

	@Override
	public void addAll(MultiMap<? extends K, ? extends V> map) {
		Objects.requireNonNull(map).forEach(this::add);
	}

	@Override
	public Collection<V> allValues() {
		return Collections.unmodifiableCollection(asCollection());
	}
	
	private Collection<V> asCollection() {
		return new AbstractCollection<V>() {
			@Override
			public Iterator<V> iterator() {
				return new Iterator<V>() {
					private Iterator<LinkedHashSet<V>> entryIt = values.values().iterator();
					private Iterator<V> valuesIt = initValue();
					
					private Iterator<V> initValue() {
						if (entryIt.hasNext()) {
							return entryIt.next().iterator();
						}
						return Collections.emptyIterator();
					}
					
					@Override
					public boolean hasNext() {
						return valuesIt.hasNext();
					}

					@Override
					public V next() {
						if (!hasNext()) {
							throw new NoSuchElementException();
						}
						V tmp = valuesIt.next();
						if (!valuesIt.hasNext() && entryIt.hasNext()) {
							valuesIt = entryIt.next().iterator();
						}
						return tmp;
					}
				};
			}

			@Override
			public int size() {
				return MultiMapMap.this.size();
			}
		};
	}
	
	public Map<K, V> asMapOfLastValues() {
		return Collections.unmodifiableMap(new AbstractMap<K, V>() {

			@Override
			public Set<Entry<K, V>> entrySet() {
				return values.entrySet().stream()
					.map(e -> new SimpleEntry<>(e.getKey(), lastValueOfEntry(e)))
					.collect(Collectors.toCollection(() -> new LinkedHashSet<Entry<K, V>>()));
			}
			
			private V lastValueOfEntry(Entry<K, LinkedHashSet<V>> entry) {
				Iterator<V> it = entry.getValue().iterator();
				V value = null;
				while(it.hasNext()) {
					value = it.next();
				}
				return value;
			}
		});
	}

}
