package fr.umlv.movies;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VideoLibrary {

	public static void doOnMovieFile(Consumer<Stream<String>> consumer) throws IOException {
		try (Stream<String> lines = Files.lines(Paths.get("movies.txt"))) {
			consumer.accept(lines);
		}
	}

	public static <R> R computeOnMovieFile(Function<Stream<String>, R> fun) throws IOException {
		try (Stream<String> lines = Files.lines(Paths.get("movies.txt"))) {
			return fun.apply(lines);
		}
	}

	public static Function<Stream<String>, Map<String, List<String>>> getActorsByMovies() {
		return lines -> lines.map(s -> s.split(";"))
			.collect(Collectors.toMap(array -> array[0], array -> List.of(Arrays.copyOfRange(array, 1, array.length))));
	}

	public static Consumer<Stream<String>> printActors() {
		return lines -> {
			System.out.println("\n--------------------\n50 first actors\n--------------------");
			lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1))
			.limit(50)
			.forEach(s -> System.out.println(s));
		};
	}

	public static Consumer<Stream<String>> computeAndDisplayActorsNumber() {
		return (lines) -> {
			long nbActors = lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1)).count();
			if (nbActors > 1)
				System.out.println(String.format("There are %d actors", nbActors));
			else
				System.out.println(String.format("There is %d actor", nbActors));
		};
	}

	public static Consumer<Stream<String>> computeAndDisplayDistinctActorsNumber() {
		return lines -> {
			int nbActors = lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1))
				.collect(Collectors.toSet())
				.size();
			if (nbActors > 1)
				System.out.println(String.format("There are %d distinct actors", nbActors));
			else
				System.out.println(String.format("There is %d actor", nbActors));
		};
	}

	public static Consumer<Stream<String>> computeAndDisplayDistinctActorsNumber2() {
		return lines -> {
			long nbActors = lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1))
				.distinct().count();
			if (nbActors > 1)
				System.out.println(String.format("There are %d distinct actors", nbActors));
			else
				System.out.println(String.format("There is %d actor", nbActors));
		};
	}

	public static Function<Stream<String>, Map<String, Long>> moviesByActors() {
		return lines -> lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1))
			.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}

	public static Function<Stream<String>, String> getMostActiveActor() {
		return lines -> lines.flatMap(s -> Arrays.stream(s.split(";")).skip(1))
			.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
			.entrySet().stream()
			.collect(Collectors.maxBy((e1, e2) -> Long.compare(e1.getValue(), e2.getValue())))
			.orElseThrow(() -> new NoSuchElementException("No actor was founded")).getKey();
	}

	public static void main(String[] args) throws IOException {
		Map<String, List<String>> movies = computeOnMovieFile(getActorsByMovies());
		movies.forEach((k, v) -> System.out.println(k + " : " + v));

		doOnMovieFile(printActors());
		
		System.out.println("\n-------------------------");
		doOnMovieFile(computeAndDisplayActorsNumber());
		doOnMovieFile(computeAndDisplayDistinctActorsNumber());
		doOnMovieFile(computeAndDisplayDistinctActorsNumber2());

		System.out.println(String.format("Brad Pitt played in %d movies",
			computeOnMovieFile(moviesByActors()).getOrDefault("Brad Pitt", -1L)));
		System.out.println(String.format("The most active actor is %s", computeOnMovieFile(getMostActiveActor())));
	}
}
