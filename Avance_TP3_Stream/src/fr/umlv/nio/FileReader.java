package fr.umlv.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileReader {
	private static final String FILE_URI = "movies.txt";
	
	public static void readFile1(String uri) {
		Path path = Paths.get(uri);
		try {
			Files.lines(path).forEach(line -> System.out.println(line));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void readFile2(String uri) {
		Path path = Paths.get(uri);
		try {
			Stream<String> lines = Files.lines(path);
			lines.forEach(line -> System.out.println(line));
			lines.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void readFile3(String uri) {
		Path path = Paths.get(uri);
		Stream<String> lines = null;
		try {
			lines = Files.lines(path);
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (lines != null) lines.close();
		}
	}
	
	public static void readFile4(String uri) {
		Path path = Paths.get(uri);
		Stream<String> lines = Stream.empty();
		try {
			lines = Files.lines(path);
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			lines.close();
		}
	}
	
	public static void readFile5(String uri) throws IOException {
		Path path = Paths.get(uri);
		Stream<String> lines = Stream.empty();
		try {
			lines = Files.lines(path);
			lines.forEach(line -> System.out.println(line));
		} catch (IOException e) {
			throw e;
		} finally {
			lines.close();
		}
	}
	
	public static void readFile6(String uri) throws IOException {
		Path path = Paths.get(uri);
		try (Stream<String> lines = Files.lines(path)) {
			lines.forEach(line -> System.out.println(line));
		}
	}

	public static void main(String[] args) throws IOException {
		readFile1(FILE_URI);
		readFile2(FILE_URI);
		readFile3(FILE_URI);
		readFile4(FILE_URI);
		readFile5(FILE_URI);
		readFile6(FILE_URI);
	}
}
