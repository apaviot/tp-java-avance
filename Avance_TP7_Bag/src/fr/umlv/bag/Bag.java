package fr.umlv.bag;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public interface Bag<E> extends Iterable<E> {
	public int add(E element, int count);
	public int count(Object element);
	public Collection<E> asCollection();
	
	public static <E> Bag<E> createSimpleBag() {
		return new BagImpl<E>();
	}
	
	public static <E> Bag<E> createOrderedByInsertionBag() {
		return new BagImpl<E>(new LinkedHashMap<>());
	}
	
	public static <E> Bag<E> createOrderedByElementBag(Comparator<E> comparator) {
		return new BagImpl<E>(new TreeMap<>(comparator));
	}
	
	public static <E extends Comparable<? super E>> Bag<E> createOrderedByElementBagFromCollection(Collection<E> collection) {
		Bag<E> bag = createOrderedByElementBag(Comparator.<E>naturalOrder());
		collection.forEach(el -> bag.add(el, 1));
		return bag;
	}
}
