package fr.umlv.bag;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

class BagImpl<E> implements Bag<E> {
	private final Map<E, Integer> elements;
	private int size;
	
	public BagImpl() {
		this(new HashMap<>());
	}
	
	public BagImpl(Map<E, Integer> map) {
		elements = Objects.requireNonNull(map);
	}

	@Override
	public int add(E element, int count) {
		Objects.requireNonNull(element);
		requirePositive(count);
		size += count;
		//return elements.compute(element, (k, v) -> (v == null) ? count : v + count);
		return elements.merge(element, count, (old, c) -> old + c);
	}
	
	private static int requirePositive(int value) {
		if (value > 0) {
			return value;
		}
		throw new IllegalArgumentException("The value is expected to be strictly positive : " + value);
	}

	@Override
	public int count(Object element) {
		return elements.getOrDefault(Objects.requireNonNull(element), 0);
	}
	
	@Override
	public void forEach(Consumer<? super E> consumer) {
		Objects.requireNonNull(consumer);
		asStream().forEach(consumer::accept);
	}

	@Override
	public Iterator<E> iterator() {
		//return new BagIterator();
		//return asStream().iterator();
		
		return new Iterator<E>() {
			Iterator<Map.Entry<E, Integer>> entryIt = elements.entrySet().iterator();
			E current = null;
			int elementsLeft = 0;

			@Override
			public boolean hasNext() {
				return elementsLeft != 0 || entryIt.hasNext();
			}

			@Override
			public E next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				if (elementsLeft <= 0) {
					Map.Entry<E, Integer> entry = entryIt.next();
					current = entry.getKey();
					elementsLeft = entry.getValue();
				}
				elementsLeft--;
				return current;
			}
		};
	}
	
	private Stream<E> asStream() {
		return elements.entrySet().stream()
			.flatMap(entry -> Collections.nCopies(entry.getValue(), entry.getKey()).stream());
	}
	
	@Override
	public Collection<E> asCollection() {
		return new AbstractCollection<E>() {
			@Override
			public Iterator<E> iterator() {
				return BagImpl.this.iterator();
			}

			@Override
			public int size() {
				return size;
			}
			
			@Override
			public boolean contains(Object obj) {
				return elements.getOrDefault(Objects.requireNonNull(obj), 0) > 0;
			}
		};
	}
	
	private class BagIterator implements Iterator<E> {
		private final Iterator<E> keyIterator;
		private E currentElement;
		private int elementsLeft;
		
		BagIterator() {
			keyIterator = elements.keySet().iterator();
			if (keyIterator.hasNext()) {
				currentElement = keyIterator.next();
				elementsLeft = elements.get(currentElement);
			}
		}
		
		@Override
		public boolean hasNext() {
			return elementsLeft > 0;
		}
		
		@Override
		public E next() {
			if (hasNext()) {
				E tmp = currentElement;
				elementsLeft--;
				if (elementsLeft <= 0 && keyIterator.hasNext()) {
					currentElement = keyIterator.next();
					elementsLeft = elements.get(currentElement);
				}
				return tmp;
			}
			throw new NoSuchElementException();
		}		
	}

}
