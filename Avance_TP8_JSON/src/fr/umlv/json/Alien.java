package fr.umlv.json;

public class Alien {

	private final String planet;
	private final int age;

	public Alien(String planet, int age) {
		super();
		this.planet = planet;
		this.age = age;
	}

	@JSONProperty
	public String getPlanet() {
		return planet;
	}

	@JSONProperty
	public int getAge() {
		return age;
	}

}
