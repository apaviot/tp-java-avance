package fr.umlv.json;

public class Person {

	private final String firstName;
	private final String lastName;

	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@JSONProperty(name = "first-name")
	public String getFirstName() {
		return firstName;
	}

	@JSONProperty(name = "last-name")
	public String getLastName() {
		return lastName;
	}

}
