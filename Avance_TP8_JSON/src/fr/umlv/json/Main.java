package fr.umlv.json;

public class Main {

	public static void main(String args[]) {
		Person person = new Person("John", "Doe");
		System.out.println(JSONEncoder.toJSON(person));
		
		Alien alien = new Alien("E.T.", 100);
		System.out.println(JSONEncoder.toJSON(alien));
	}
}
