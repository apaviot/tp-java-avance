package fr.umlv.json;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class JSONEncoder {
	
	private final static ClassValue<Map<String, Function<Object, String>>> classValue = new ClassValue<>() {
		@Override
		protected Map<String, Function<Object, String>> computeValue(Class<?> clazz) {
			return Arrays.stream(clazz.getMethods())
				.filter(method -> method.isAnnotationPresent(JSONProperty.class))
				.collect(Collectors.toMap(m -> propertyName(m), m -> o -> String.valueOf(tryInvoke(m, o))));
		}
	};
	
	public static String toJSON(Object obj) {
		Objects.requireNonNull(obj);
		return classValue.get(obj.getClass()).entrySet().stream()
			.map(entry -> "\"" + entry.getKey() + "\" : \"" + entry.getValue().apply(obj) + "\"")
			.collect(Collectors.joining(",\n    ", "{\n    ", "\n}"));
	}
	
	private static Object tryInvoke(Method method, Object obj) {
		try {
			return method.invoke(obj);
		} catch (IllegalAccessException e) {
			throw new IllegalStateException(e);
		} catch(InvocationTargetException e) {
			Throwable cause = e.getCause();
			if (cause instanceof RuntimeException) {
				throw (RuntimeException) cause;
			}
			if (cause instanceof Error) {
				throw (Error) cause;
			}
			throw new UndeclaredThrowableException(cause);
		}
	}
	
	private static String propertyName(Method method) {
		JSONProperty annotation = method.getAnnotation(JSONProperty.class);
		if (annotation.name().isEmpty()) {
			return propertyName(method.getName());
		}
		return annotation.name();
	}
	
	private static String propertyName(String name) {
		return Character.toLowerCase(name.charAt(3)) + name.substring(4);
	}
}