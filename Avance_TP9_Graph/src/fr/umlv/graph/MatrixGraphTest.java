package fr.umlv.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;

@SuppressWarnings("static-method")
public class MatrixGraphTest {
  @Test(expected = IllegalArgumentException.class)
  public void testInvalidNodeCount() {
    new MatrixGraph<>(-17);
  }

  @Test
  public void testGetWeightEmpty() {
    int nodeCount = 20;
    MatrixGraph<Object> graph = new MatrixGraph<>(nodeCount);
    for (int i = 0; i < nodeCount; i++) {
      for (int j = 0; j < nodeCount; j++) {
        assertFalse(graph.getWeight(i, j).isPresent());
      }
    }
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testHasEdgeValid1() {
    MatrixGraph<Object> graph = new MatrixGraph<>(5);
    graph.getWeight(-1, 3);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testHasEdgeValid2() {
    MatrixGraph<Object> graph = new MatrixGraph<>(5);
    graph.getWeight(2, -1);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testHasEdgeValid3() {
    MatrixGraph<Object> graph = new MatrixGraph<>(5);
    graph.getWeight(5, 2);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testHasEdgeValid4() {
    MatrixGraph<Object> graph = new MatrixGraph<>(5);
    graph.getWeight(3, 5);
  }

  @Test(expected = NullPointerException.class)
  public void testAddEdgeNullWeight() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(7);
    graph.addEdge(3, 4, null);
  }

  @Test
  public void testAddEdge() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(7);
    graph.addEdge(3, 4, 2);
    assertEquals(2, (int) graph.getWeight(3, 4).get());
    assertFalse(graph.getWeight(4, 3).isPresent());
  }

  @Test
  public void testAddEdgeTwice() {
    MatrixGraph<String> graph = new MatrixGraph<>(7);
    graph.addEdge(3, 4, "foo");
    graph.addEdge(3, 4, "bar");
    assertEquals("bar", graph.getWeight(3, 4).get());
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testAddEdgeValid1() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(5);
    graph.addEdge(-1, 3, 7);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testAddEdgeValid2() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(5);
    graph.addEdge(2, -1, 8);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testAddEdgeValid3() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(5);
    graph.addEdge(5, 2, 9);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testAddEdgeValid4() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(5);
    graph.addEdge(3, 5, 10);
  }

  @Test
  public void testAddEdgeALot() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(17);
    ThreadLocalRandom random = ThreadLocalRandom.current();
    IntStream.range(0, 1000).forEach(index -> {
      int i = random.nextInt(17);
      int j = random.nextInt(17);
      int value = random.nextInt(10_000) - 5_000;
      graph.addEdge(i, j, value);
      assertEquals(value, (int) graph.getWeight(i, j).get());
    });
  }

  @Test
  public void testNoEdges() {
    MatrixGraph<String> graph = new MatrixGraph<>(17);
    graph.edges(0, (src, dst, weight) -> fail());
  }

  @Test(expected = NullPointerException.class)
  public void testEdgesNullConsumer() {
    new MatrixGraph<String>(17).edges(0, null);
  }

  @Test
  public void testEdgesOneEdge() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(3);
    graph.addEdge(1, 0, 2);
    graph.edges(0, (src, dst, weight) -> {
      assertEquals(1, src);
      assertEquals(0, dst);
      assertEquals(2, (int) weight);
    });
  }

  @Test(timeout = 5000)
  public void testEdgesALot() {
    int nodeCount = 200;
    MatrixGraph<Integer> graph = new MatrixGraph<>(nodeCount);
    for (int i = 0; i < nodeCount; i++) {
      for (int j = 0; j < nodeCount; j++) {
        graph.addEdge(i, j, i % 10 + j);
      }
    }
    for (int i = 0; i < nodeCount; i++) {
      graph.edges(i, (src, dst, weight) -> {
        assertEquals(src % 10 + dst, (int) weight);
      });
    }
  }

  @Test
  public void testNeighborsEmptyHasNext() {
    MatrixGraph<Object> graph = new MatrixGraph<>(6);
    Iterator<Integer> iterator = graph.neighborIterator(0);
    assertFalse(iterator.hasNext());
    assertFalse(iterator.hasNext());
    assertFalse(iterator.hasNext());
  }

  @Test(expected = NoSuchElementException.class)
  public void testNeighborsEmptyNext() {
    MatrixGraph<Object> graph = new MatrixGraph<>(6);
    graph.neighborIterator(0).next();
  }

  @Test
  public void testNeighborsOneEdge() {
    MatrixGraph<String> graph = new MatrixGraph<>(6);
    graph.addEdge(1, 2, "hello");
    Iterator<Integer> iterator = graph.neighborIterator(1);
    assertTrue(iterator.hasNext());
    assertEquals(2, (int) iterator.next());
    assertFalse(iterator.hasNext());
    try {
      iterator.next();
      fail();
    } catch (NoSuchElementException e) {
      // ok !
    }
  }

  @Test
  public void testNeighborsNoHasNext() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(10);
    for (int i = 0; i < 10; i++) {
      graph.addEdge(5, i, -1);
    }

    HashSet<Integer> result = new HashSet<>();
    HashSet<Integer> expected = new HashSet<>();
    Iterator<Integer> iterator = graph.neighborIterator(5);
    for (int i = 0; i < 10; i++) {
      expected.add(i);
      result.add(iterator.next());
    }
    assertEquals(expected, result);

    assertFalse(iterator.hasNext());
    try {
      iterator.next();
      fail();
    } catch (NoSuchElementException e) {
      // ok !
    }
  }

  @Test(timeout = 5000)
  public void testNeighborsNonDestructive() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(12);
    for (int i = 0; i < 12; i++) {
      graph.addEdge(5, i, 67);
    }
    Iterator<Integer> neighbors = graph.neighborIterator(5);
    while (neighbors.hasNext()) {
      neighbors.next();
    }
    for (int i = 0; i < 12; i++) {
      assertEquals(67, (int) graph.getWeight(5, i).get());
    }
  }

  @Test(timeout = 5000)
  public void testNeighborSeveralHasNext() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(14);
    graph.addEdge(3, 7, 2);
    graph.addEdge(3, 5, 3);
    graph.addEdge(7, 3, 4);
    Iterator<Integer> neighbors = graph.neighborIterator(3);
    assertTrue(neighbors.hasNext());
    int vertex1 = neighbors.next();
    for (int i = 0; i < 5; i++) {
      assertTrue(neighbors.hasNext());
    }
    int vertex2 = neighbors.next();
    assertFalse(neighbors.hasNext());
    assertTrue((vertex1 == 5 && vertex2 == 7) || (vertex1 == 7 && vertex2 == 5));
  }

  @Test
  public void testIteratorRemove() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(11);
    graph.addEdge(3, 10, 13);
    Iterator<Integer> neighbors = graph.neighborIterator(3);
    assertEquals(10, (int) neighbors.next());
    neighbors.remove();
    assertFalse(graph.getWeight(3, 10).isPresent());
  }

  @Test(expected = IllegalStateException.class)
  public void testIteratorRemoveInvalid() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(21);
    graph.addEdge(20, 19, 20);
    Iterator<Integer> neighbors = graph.neighborIterator(20);
    neighbors.remove();
  }

  @Test
  public void testIteratorRemoveTwiceInvalid() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(21);
    graph.addEdge(20, 19, 20);
    Iterator<Integer> neighbors = graph.neighborIterator(20);
    neighbors.next();
    neighbors.remove();
    assertFalse(graph.getWeight(20, 19).isPresent());
    try {
      neighbors.remove();
      fail();
    } catch (IllegalStateException e) {
      // ok !
    }
  }

  @Test
  public void testIteratorRemoveALot() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(50);
    for (int i = 0; i < 50; i++) {
      for (int j = 0; j < i; j++) {
        graph.addEdge(i, j, i + j);
      }
    }

    for (int i = 0; i < 50; i++) {
      Iterator<Integer> neighbors = graph.neighborIterator(i);
      for (int j = 0; j < i; j++) {
        assertTrue(neighbors.hasNext());
        neighbors.next();
        neighbors.remove();
      }
      assertFalse(neighbors.hasNext());
    }

    for (int i = 0; i < 50; i++) {
      for (int j = 0; j < 50; j++) {
        assertFalse(graph.getWeight(i, j).isPresent());
      }
    }
  }

  @Test
  public void testNeighborStream() {
    MatrixGraph<String> graph = new MatrixGraph<>(17);
    assertEquals(graph.neighborStream(0).count(), 0);
  }

  @Test
  public void testNeighborStreamOneEdge() {
    MatrixGraph<Integer> graph = new MatrixGraph<>(3);
    graph.addEdge(1, 2, 3);
    assertEquals(graph.neighborStream(1).boxed().collect(Collectors.toSet()), Collections.singleton(2));
  }

  @Test(timeout = 5000)
  public void testNeighborStreamALot() {
    int nodeCount = 200;
    MatrixGraph<Boolean> graph = new MatrixGraph<>(nodeCount);
    for (int i = 0; i < nodeCount; i++) {
      for (int j = 0; j < nodeCount; j++) {
        graph.addEdge(i, j, true);
      }
    }
    for (int i = 0; i < nodeCount; i++) {
      assertEquals(nodeCount, graph.neighborStream(i).distinct().count());
    }
  }

}
