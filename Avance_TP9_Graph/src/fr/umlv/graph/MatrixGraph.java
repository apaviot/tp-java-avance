package fr.umlv.graph;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

public class MatrixGraph<E> implements Graph<E> {
	
	private final Object arcs[];
	final int nodeCount;
	
	public MatrixGraph(int nodeCount) {
		requirePositive(nodeCount);
		arcs = new Object[nodeCount * nodeCount];
		this.nodeCount = nodeCount;
	}
	
	private static int requirePositive(int value) {
		if (value > 0) {
			return value;
		}
		throw new IllegalArgumentException("The following value is expected to be positive : " + value);
	}
	
	private int checkIndex(int value) {
		if (value >= 0 && value < nodeCount) {
			return value;
		}
		throw new IndexOutOfBoundsException("The following index is invalid : " + value);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Optional<E> getWeight(int src, int dst) {
		return Optional.ofNullable((E) arcs[checkIndex(src) * nodeCount + checkIndex(dst)]);
	}

	@Override
	public void addEdge(int src, int dst, E weight) {
		arcs[checkIndex(src) * nodeCount + checkIndex(dst)] = Objects.requireNonNull(weight);
	}

	@Override
	public Iterator<Integer> neighborIterator(int src) {
		return new MatrixGraphIterator(checkIndex(src));
	}
	
	private class MatrixGraphIterator implements Iterator<Integer>{
		
		private final int src;
		private int current;
		private boolean used = false;
		private int previous;
		
		MatrixGraphIterator(int src) {
			this.src = src;
			current = 0;
			used = true;
			previous = -1;
		}

		@Override
		public boolean hasNext() {
			if (!used) {
				return true;
			}
			while (current < nodeCount) {
				if (arcs[src * nodeCount + current] != null) {
					return true;
				}
				current++;
			}
			return false;
		}

		@Override
		public Integer next() {
			if (hasNext()) {
				used = true;
				previous = current;
				return current++;
			}
			throw new NoSuchElementException();
		}
		
		@Override
		public void remove() {
			if (previous != -1) {
				arcs[src * nodeCount + previous] = null;
				previous = -1;
			}
			else
				throw new IllegalStateException();
		}
	}
}
