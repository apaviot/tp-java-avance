package fr.umlv.graph;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

public class NodeMapGraph<E> implements Graph<E> {
	
	private final Map<Integer, E>[] arcs;
	public final int nodeCount;
	
	@SuppressWarnings("unchecked")
	public NodeMapGraph(int nodeCount) {
		requirePositive(nodeCount);
		arcs = new Map[nodeCount];
		this.nodeCount = nodeCount;
	}
	
	private static int requirePositive(int value) {
		if (value > 0)
			return value;
		throw new IllegalArgumentException("The following value is expected to be positive : " + value);
	}
	
	private int checkIndex(int value) {
		if (value >= 0 && value < nodeCount)
			return value;
		throw new IndexOutOfBoundsException("The following index is invalid : " + value);
	}

	@Override
	public Optional<E> getWeight(int src, int dst) {
		checkIndex(dst);
		if (arcs[checkIndex(src)] != null)
			return Optional.ofNullable((E) arcs[src].get(dst));
		return Optional.empty();
	}

	@Override
	public void addEdge(int src, int dst, E weight) {
		if (arcs[checkIndex(src)] == null)
			arcs[src] = new HashMap<Integer, E>();
		arcs[src].put(checkIndex(dst), Objects.requireNonNull(weight));
	}

	@Override
	public Iterator<Integer> neighborIterator(int src) {
		if (arcs[checkIndex(src)] != null)
			return arcs[checkIndex(src)].keySet().iterator();
		return new EmptyIterator();
	}
	
	private class EmptyIterator implements Iterator<Integer> {

		@Override
		public boolean hasNext() {
			return false;
		}

		@Override
		public Integer next() {
			throw new NoSuchElementException();
		}
	}
}
